import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}


// apart from documentation i've got one more simple way to config
// i.e install dotenv package  then use the @config() import it and ypu good to go
// eg process.env.YOUR_VAR_NAME  THIS ONE I SPECIFIED IN USERMANAGEMENT PROJ