import { Injectable } from '@nestjs/common';
import {ConfigService } from '@nestjs/config';

@Injectable()
export class AppService {

constructor(private service:ConfigService){}

key = this.service.get<string>('DD'); // it can also take optional second argument
  

  getHello(): string {
    console.log(this.key);
    return 'Hello World!';
  }
}
