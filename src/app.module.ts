import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {ConfigModule } from '@nestjs/config';
import configuration from './configuration';

@Module({
  imports: [ConfigModule.forRoot()], // this uses dotenv internally // we can also use forFeature for featureModules
  // imports: [ConfigModule.forRoot({load:[configuration,newconfig ...etc]})], //multiple files can be loaded here
  // imports: [ConfigModule.forRoot({
  //   isGlobal:true
  // })],by doing this you'll set it globally then  no need for configservice anywhere
  controllers: [AppController], // go check app.service file
  providers: [AppService],
})
export class AppModule {}
